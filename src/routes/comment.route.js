const express = require("express");
const router = express.Router();
const commentController = require("../controller/comment.controller");
const { authMiddleware } = require("../middlewares/authMiddleware");

router.get("/api/comment", commentController.getComment);
router.get("/api/comment/:id", commentController.getCommentById);
router.post("/api/comment", 
authMiddleware,
commentController.createComment);
router.put("/api/comment", commentController.updateComment);
router.delete("/api/comment/:id", commentController.deleteComment);

module.exports = router;