CREATE DATABASE platinum;


CREATE TABLE category
(
    category_id bigint NOT NULL DEFAULT nextval('category_category_id_seq'::regclass),
    is_delete boolean NOT NULL,
    category_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT category_pkey PRIMARY KEY (category_id)
)




CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(15) NOT NULL UNIQUE,
  password VARCHAR(255) NOT NULL,
  is_admin VARCHAR(15) DEFAULT 'user' CHECK (is_admin IN ('user', 'author')),
  email VARCHAR(255) NOT NULL UNIQUE,
  phone_number VARCHAR(12) NOT NULL,
  created_at TIMESTAMPTZ DEFAULT current_timestamp,
  updated_at TIMESTAMPTZ DEFAULT current_timestamp,
  deleted_at TIMESTAMPTZ,
  is_delete boolean NOT NULL DEFAULT false
);

CREATE TABLE comment (
    comment_id bigint NOT NULL DEFAULT nextval('comment_comment_id_seq'::regclass),
    detail_comment text COLLATE pg_catalog."default" NOT NULL,
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    modified_at timestamp without time zone NOT NULL DEFAULT now(),
    is_delete boolean NOT NULL,
    news_id integer,
    id integer,
    CONSTRAINT comment_pkey PRIMARY KEY (comment_id),
    CONSTRAINT fk_news FOREIGN KEY (news_id)
        REFERENCES public.news (news_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_user FOREIGN KEY (id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE news (
    news_id bigint NOT NULL DEFAULT nextval('news_news_id_seq'::regclass),
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default" NOT NULL,
    detail text COLLATE pg_catalog."default" NOT NULL,
    image text COLLATE pg_catalog."default" NOT NULL,
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    modified_at timestamp without time zone NOT NULL DEFAULT now(),
    recommendation boolean NOT NULL,
    view_number integer NOT NULL,
    title_desc text COLLATE pg_catalog."default" NOT NULL,
    is_delete boolean NOT NULL DEFAULT false,
    category_id integer,
    id integer,
    CONSTRAINT news_pkey PRIMARY KEY (news_id),
    CONSTRAINT fk_category FOREIGN KEY (category_id)
        REFERENCES public.category (category_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_user FOREIGN KEY (id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION 
);


