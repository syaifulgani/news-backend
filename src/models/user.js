class User {
  constructor(users) {
    this.id = users.id;
    this.username = users.username;
    this.email = users.email;
    this.password = users.password;
    this.is_admin = users.is_admin;
    this.phone_number = users.phone_number;
    this.updated_at = users.updated_at_at;
    this.created_at = users.created_at;
    this.deleted_at = users.deleted_at;
    this.is_delete = users.is_delete;
  }
}

module.exports = User;
